<?php
$aboutMeData = array(
    "name" => "Andrianay Miarintsoa Orlando",
    "occupation" => "Full Stack & Java Developer",
    "description" => "Passionate about Artificial Intelligence. Passionate about coding and application design. Dynamic and enjoys working as a team player.",
    "birthday" => "3 September 2002",
    "college" => "IT University",
    "phone" => "+261 32 74 373 24",
    "city" => "Antananarivo, Madagascar",
    "age" => 22,
    "degree" => "Master I",
    "email" => "andrianayorllando@gmail.com",
    "freelance" => "Available"
);
?>
