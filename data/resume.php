<?php
$resumeData = array(
    "summary" => array(
        "name" => "Andrianay Miarintsoa Orlando",
        "description" => "Passionate about Artificial Intelligence. Passionate about coding and application design. Dynamic and enjoys working as a team player.",
        "address" => "Lot II T 105 B, Iaviloha",
        "phone" => "+261 32 74 373 24",
        "email" => "andrianayorllando@gmail.com"
    ),
    "education" => array(
        array(
            "degree" => "Master I",
            "year" => "2024",
            "institution" => "IT University, Andoharanofotsy, Antananarivo"
        ),
        array(
            "degree" => "Bachelor's degree with honors",
            "year" => "2020-2023",
            "institution" => "IT University, Andoharanofotsy, Antananarivo",
            "description" => "Bachelor's degree from IT University, development options."
        ),
        array(
            "degree" => "Scientific baccalaureate with honours",
            "year" => "2019",
            "institution" => "For Twelve School, Ambany Atsimo"
        )
    ),
    "experience" => array(
        array(
            "position" => "FULL Stack JS",
            "duration" => "2024-Now",
            "company" => "Only Travaux, France",
            "responsibilities" => array(
                "Code re-engineering: Optimisation and refactoring of ReactJS code to improve performance and maintainability.",
                "Development of advanced functionalities: Design and implementation of new functionalities for the Friendw tourism site, increasing user engagement.",
                "Back-end intervention: Optimisation of back-end processes for more efficient data processing and improved application responsiveness.",
                "Visual design: Creation of mock-ups and visual prototypes using Figma to enhance the user experience."
            )
        ),
        array(
            "position" => "FULL Stack Developer",
            "duration" => "2023 - 2024",
            "company" => "Hoag Target, Behoririka",
            "responsibilities" => array(
                "Had a role as a DBA Developer (Manage Databases)",
                "Developed a payment system in their website, and its management.",
                "Attendance Management based on facial recognition",
                "Designed databases for all kinds of projects."
            )
        ),
        array(
            "position" => "Application designer & Framework creator",
            "duration" => "2020-2023",
            "company" => "IT University",
            "responsibilities" => array(
                "Settled down a homemade ORM in Java",
                "Application design & Realisation of E-Print",
                "Chess Game in Java",
                "Homemade Scaffold in React & Java"
            )
        )
    )
);
?>
